import axios from "axios";
import constants from "../constants/constants";

const generoApi = axios.create({
  baseURL: "http://localhost:3001",
});

generoApi.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem(constants.Token);
    if (token && config.headers) {
      config.headers[constants.Authorization] = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default generoApi;
