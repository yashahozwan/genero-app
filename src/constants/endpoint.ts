const endpoint = {
  root: "/",
  signIn: "/signin",
  signUp: "/signup",
  tester: "/tester",
  users: "/users",
  enable: "/enable",
  profile: "/profile",
  notFound: "/*",
  hplc: "/hplc",
  liveDisplay: "/live-display",
  history: "/history",
  hplcUpdate: "/hplc-update",
};

export default endpoint;
