const constants = {
  Token: "token",
  Authorization: "Authorization",
  ADMIN: "admin",
  HPLC01: "HPLC-01",
  HPLC02: "HPLC-02",
  HPLC03: "HPLC-03",
  HPLC04: "HPLC-04",
  ONGOING: "Ongoing",
  QUEUE: "Queue",
  FINISH: "Finish",
};

export const titleTableHead = [
  "Id",
  "HPLC Type",
  "Tester",
  "Start",
  "End",
  "Sample",
  "LOT",
  "Status",
];

export default constants;
