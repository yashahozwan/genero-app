import React, { Fragment, useEffect } from "react";
import { Route, Routes } from "react-router-dom";
import endpoint from "./constants/endpoint";
import SignIn from "./features/signin/SignIn";
import Home from "./features/home/Home";
import SignUp from "./features/signup/SignUp";
import Tester from "./features/tester/Tester";
import Enable from "./features/enable/Enable";
import { useDispatch, useSelector } from "react-redux";
import NotFound from "./features/not-found/NotFound";
import Profile from "./features/profile/Profile";
import { getPageSignup, selectAllPagesData } from "./store/pageSignupSlice";
import { AppDispatch } from "./store/configureStore";
import Users from "./features/users/Users";
import LiveDisplay from "./features/live-display/LiveDisplay";
import Hplc from "./features/hplc/Hplc";
import History from "./features/history/History";
import HplcUpdate from "./features/hplc-update/HplcUpdate";

const App = () => {
  const dispatch = useDispatch<AppDispatch>();
  const pagesResponse = useSelector(selectAllPagesData);

  useEffect(() => {
    dispatch(getPageSignup());
  }, []);

  return (
    <Fragment>
      <Routes>
        <Route path={endpoint.root} element={<Home />}>
          <Route path={endpoint.hplc} element={<Hplc />} />
          <Route path={endpoint.history} element={<History />} />
          <Route path={endpoint.profile} element={<Profile />} />
          <Route path={endpoint.tester} element={<Tester />} />
          <Route path={endpoint.users} element={<Users />} />
          <Route path={endpoint.enable} element={<Enable />} />
          <Route path={endpoint.hplcUpdate} element={<HplcUpdate />} />
        </Route>
        <Route path={endpoint.liveDisplay} element={<LiveDisplay />} />
        <Route path={endpoint.signIn} element={<SignIn />} />
        {pagesResponse.data.isEnable && (
          <Route path={endpoint.signUp} element={<SignUp />} />
        )}
        <Route path={endpoint.notFound} element={<NotFound />} />
      </Routes>
    </Fragment>
  );
};

export default App;
