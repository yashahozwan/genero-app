interface AuthResponse {
  loading: boolean;
  error: boolean;
  message: string | null;
  data: {
    token: string;
    id: number;
    name: string;
    email: string;
    password: string;
    createdAt: string;
  };
}

export default AuthResponse;
