interface PagesResponse {
  loading: boolean;
  error: boolean;
  message: string | null;
  data: {
    id: number;
    name: string;
    isEnable: boolean;
  };
}

export default PagesResponse;
