import TypeTester from "../types/TypeTester";

interface TestersResponse {
  loading: boolean;
  error: boolean;
  message: string | null;
  data: TypeTester[];
}

export default TestersResponse;
