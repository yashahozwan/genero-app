import TypeUser from "../types/TypeUser";

interface UsersResponse {
  loading: boolean;
  error: boolean;
  message: string | null;
  data: {
    id: number;
    name: string;
    email: string;
    createdAt: string;
    role: string;
  };
  users: TypeUser[];
}

export default UsersResponse;
