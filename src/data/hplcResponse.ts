import TypeHplc from "../types/TypeHplc";

interface HplcResponse {
  loading: boolean;
  error: string;
  message: string;
  data: {};
  hplcList: TypeHplc[];
}

export default HplcResponse;
