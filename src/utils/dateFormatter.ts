import moment from "moment";

moment().isUTC();
const dateFormatter = (date: string) => {
  return moment(Number(date)).format("LLLL");
};

export default dateFormatter;
