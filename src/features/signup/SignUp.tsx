import "./SignUp.css";
import React, { SyntheticEvent, useEffect, useState } from "react";
import Logo from "../../assets/image/logo.png";
import AuthResponse from "../../data/authResponse";
import constants from "../../constants/constants";
import endpoint from "../../constants/endpoint";
import { Alert, Button, Form, FormControl, InputGroup } from "react-bootstrap";
import { Link, Navigate, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch } from "../../store/configureStore";
import { authCleaning, authSignOut, authSignUp } from "../../store/authSlice";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";

const SignUp: React.FC = () => {
  const token = localStorage.getItem(constants.Token);
  const dispatch = useDispatch<AppDispatch>();
  const authResponse = useSelector(
    (state: { auth: AuthResponse }) => state.auth
  );
  const navigate = useNavigate();

  const [name, setName] = useState<string>("");
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [isVisible, setIsVisible] = useState<boolean>(false);

  const onSubmit = (event: SyntheticEvent) => {
    event.preventDefault();
    const data = { name, email, password };
    dispatch(authSignUp(data));
    clearFormField();

    setTimeout(() => {
      if (!authResponse.error) {
        navigate(endpoint.signIn);
        dispatch(authSignOut());
      }
    }, 200);
  };

  const handleCleaning = () => {
    dispatch(authCleaning());
  };

  const clearFormField = () => {
    setName("");
    setEmail("");
    setPassword("");
  };

  useEffect(() => {
    document.title = "Sign Up";
  }, []);

  if (token) {
    return <Navigate to={endpoint.root} />;
  }

  return (
    <section className="signup-section">
      <Form onSubmit={onSubmit}>
        <img src={Logo} alt="logo" />
        {authResponse.message && authResponse.error && (
          <Alert variant="danger">{authResponse.message}</Alert>
        )}
        <Form.Group className="mb-3">
          <Form.Label>Name</Form.Label>
          <Form.Control
            type="text"
            onChange={(event) => setName(event.currentTarget.value)}
          />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Email</Form.Label>
          <Form.Control
            type="text"
            onChange={(event) => setEmail(event.currentTarget.value)}
          />
        </Form.Group>
        <Form.Group className="mb-4">
          <Form.Label>Password</Form.Label>
          <InputGroup className="mb-3">
            <FormControl
              type={!isVisible ? "password" : "text"}
              value={password}
              onChange={(event) => setPassword(event.currentTarget.value)}
              aria-describedby="basic-addon2"
            />
            <Button
              disabled={!password}
              variant="outline-primary"
              id="button-addon2"
              onClick={() => setIsVisible(!isVisible)}
            >
              <FontAwesomeIcon icon={!isVisible ? faEye : faEyeSlash} />
            </Button>
          </InputGroup>
        </Form.Group>
        <Button
          disabled={!(name && email && password)}
          className="sign-in-button mb-3"
          type="submit"
        >
          Sign Up
        </Button>
        <p className="sign-in-text-button">
          <Link onClick={handleCleaning} to={endpoint.signIn}>
            Sign In
          </Link>
        </p>
      </Form>
    </section>
  );
};

export default SignUp;
