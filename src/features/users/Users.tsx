import { Button, Container, Table } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { deleteUser, getUsers, selectUsersData } from "../../store/usersSlice";
import constants from "../../constants/constants";
import { Navigate, useNavigate } from "react-router-dom";
import endpoint from "../../constants/endpoint";
import { useEffect } from "react";
import { AppDispatch } from "../../store/configureStore";
import dateFormatter from "../../utils/dateFormatter";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/free-solid-svg-icons";

const tableHead = ["Name", "Email", "Created At", "Action"];

const Users: React.FC = () => {
  const usersResponse = useSelector(selectUsersData);
  const dispatch = useDispatch<AppDispatch>();
  const navigate = useNavigate();

  const handleOnDelete = (id: number) => {
    const wantDeleteIt = window.confirm("Delete this user?");
    if (wantDeleteIt) {
      dispatch(deleteUser(id));
      setTimeout(() => {
        dispatch(getUsers());
      }, 500);
    }
  };

  const filterUsers = usersResponse.users.filter(
    (item) => item.id !== usersResponse.data.id
  );

  const renderTableHead = tableHead.map((item) => <th key={item}>{item}</th>);
  const renderListUsers = filterUsers.map((item) => (
    <tr key={item.id}>
      <td>{item.name}</td>
      <td>{item.email}</td>
      <td>{dateFormatter(item.createdAt)}</td>
      <td>
        <Button onClick={() => handleOnDelete(item.id)}>
          <FontAwesomeIcon icon={faTrash} />
        </Button>
      </td>
    </tr>
  ));

  useEffect(() => {
    document.title = "Users";
    if (usersResponse.data.role !== constants.ADMIN) {
      return navigate("/home");
    }
    dispatch(getUsers());
  }, []);

  if (usersResponse.data.role !== constants.ADMIN) {
    return <Navigate to={endpoint.root} />;
  }

  return (
    <Container>
      <h3>Manage Account</h3>
      <Table>
        <thead>
          <tr>{renderTableHead}</tr>
        </thead>
        <tbody>{renderListUsers}</tbody>
      </Table>
    </Container>
  );
};

export default Users;
