import React, { useEffect, useState } from "react";
import { Container, Form, Table } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch } from "../../store/configureStore";
import { getHplc, selectHplcData } from "../../store/hplcSlice";
import TableHeadHplc from "../../components/table-head-hplc/TableHeadHplc";
import { titleTableHead } from "../../constants/constants";

const newTitleHead = [...titleTableHead, "Created At"];

const History: React.FC = () => {
  const dispatch = useDispatch<AppDispatch>();
  const hplcResponse = useSelector(selectHplcData);
  const [sample, setSample] = useState("");

  const handleChange = () => {};

  const renderTableBody = hplcResponse.hplcList
    .filter((item) => {
      if (sample) {
        return item.lot.toLowerCase().match(sample.toLowerCase());
      } else {
        return item;
      }
    })
    .map((item) => (
      <tr key={item.id}>
        <td>{item.id}</td>
        <td>{item.hplcType}</td>
        <td>{item.tester}</td>
        <td>{item.start}</td>
        <td>{item.end}</td>
        <td>{item.sample}</td>
        <td>{item.lot}</td>
        <td>{item.status}</td>
        <td>{new Date(item.createdAt).toLocaleDateString("id")}</td>
      </tr>
    ));

  useEffect(() => {
    document.title = "History";
    dispatch(getHplc());
    handleChange();
  }, []);

  return (
    <section>
      <Container>
        <h3>History</h3>
        <Form.Group>
          <Form.Control
            onChange={(event) => setSample(event.currentTarget.value)}
            placeholder="Search by LOT"
          />
        </Form.Group>
        <Table striped bordered hover>
          <TableHeadHplc titleHead={newTitleHead} title="List Examiner" />
          <tbody>{renderTableBody}</tbody>
        </Table>
      </Container>
    </section>
  );
};

export default History;
