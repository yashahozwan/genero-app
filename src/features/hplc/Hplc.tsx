import React, { SyntheticEvent, useEffect, useState } from "react";
import { Alert, Button, Col, Container, Form, Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { getAllTesters, getTesters } from "../../store/testersSlice";
import FormControl from "../../components/form-group/FormControl";
import { AppDispatch } from "../../store/configureStore";
import { createHplc, selectHplcData } from "../../store/hplcSlice";
import constants from "../../constants/constants";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit } from "@fortawesome/free-solid-svg-icons";
import { useNavigate } from "react-router-dom";
import endpoint from "../../constants/endpoint";

const hplcList = [
  constants.HPLC01,
  constants.HPLC02,
  constants.HPLC03,
  constants.HPLC04,
];
const statusList = [constants.ONGOING, constants.QUEUE, constants.FINISH];

const Hplc: React.FC = () => {
  const testerResponse = useSelector(getAllTesters);
  const hplcResponse = useSelector(selectHplcData);
  const dispatch = useDispatch<AppDispatch>();
  const naviate = useNavigate();

  const [start, setStart] = useState("");
  const [hplcType, setHplcType] = useState("");
  const [tester, setTester] = useState("");
  const [end, setEnd] = useState("");
  const [lot, setLot] = useState("");
  const [sample, setSample] = useState("");
  const [status, setStatus] = useState("");

  const handleOnSubmit = (event: SyntheticEvent) => {
    event.preventDefault();
    const sure = window.confirm("Apakah anda yankin check kembali data hpcl");
    if (sure) {
      const data = { hplcType, tester, start, end, lot, sample, status };
      dispatch(createHplc(data));
      cleanForm();
    }
  };

  const handleDisableButton = () => {
    return !(start && hplcType && tester && end && lot && sample && status);
  };

  const cleanForm = () => {
    setHplcType("");
    setTester("");
    setStart("");
    setEnd("");
    setSample("");
    setStatus("");
  };

  const renderOptionTester = testerResponse.data.map((item) => (
    <option key={item.id}>{item.name}</option>
  ));

  const renderOptionHplc = hplcList.map((item) => (
    <option key={item} value={item}>
      {item}
    </option>
  ));

  const renderOptionStatus = statusList.map((item) => (
    <option key={item} value={item}>
      {item}
    </option>
  ));

  useEffect(() => {
    document.title = "HPLC";
    dispatch(getTesters());
  }, []);

  return (
    <section>
      <Container>
        <Button onClick={() => naviate(endpoint.hplcUpdate)}>
          <FontAwesomeIcon icon={faEdit} /> Edit
        </Button>
        <Form onSubmit={handleOnSubmit}>
          <h3 className="mb-3">Input Data HPLC</h3>
          {!hplcResponse.error && hplcResponse.message && (
            <Alert variant="success">
              <b>{hplcResponse.message}</b>
            </Alert>
          )}
          <Row sm={2}>
            <Col>
              <Form.Group className="mb-2">
                <Form.Select
                  onChange={(event) => setHplcType(event.currentTarget.value)}
                >
                  <option>HPLC Type</option>
                  {renderOptionHplc}
                </Form.Select>
              </Form.Group>

              <Form.Group className="mb-2">
                <Form.Select
                  onChange={(event) => setTester(event.currentTarget.value)}
                >
                  <option>Tester</option>
                  {renderOptionTester}
                </Form.Select>
              </Form.Group>

              <FormControl
                onChange={(event) => setStart(event.currentTarget.value)}
                value={start}
                mb={2}
                placeholder="Start"
              />

              <FormControl
                onChange={(event) => setEnd(event.currentTarget.value)}
                value={end}
                mb={2}
                placeholder="End"
              />
            </Col>
            <Col>
              <FormControl
                onChange={(event) => setLot(event.currentTarget.value)}
                value={lot}
                mb={2}
                placeholder="LOT"
              />

              <FormControl
                onChange={(event) => setSample(event.currentTarget.value)}
                value={sample}
                mb={2}
                placeholder="Sample"
              />

              <Form.Group className="mb-2">
                <Form.Select
                  onChange={(event) => setStatus(event.currentTarget.value)}
                >
                  <option>Status</option>
                  {renderOptionStatus}
                </Form.Select>
              </Form.Group>

              <div className="d-grid">
                <Button disabled={handleDisableButton()} type="submit">
                  Save
                </Button>
              </div>
            </Col>
          </Row>
        </Form>
      </Container>
    </section>
  );
};

export default Hplc;
