import "./LiveDisplay.css";
import React, { Fragment, useEffect } from "react";
import { Col, Container, ListGroup, Row } from "react-bootstrap";
import Header from "../../components/header/Header";
import { useDispatch, useSelector } from "react-redux";
import { getHplc, selectHplcData } from "../../store/hplcSlice";
import constants from "../../constants/constants";
import TypeHplc from "../../types/TypeHplc";
import { AppDispatch } from "../../store/configureStore";

const LiveDisplay: React.FC = () => {
  const hplcResponse = useSelector(selectHplcData);
  const dispatch = useDispatch<AppDispatch>();

  const hplc01 = hplcResponse.hplcList.filter(
    (item) =>
      item.hplcType === constants.HPLC01 && item.status !== constants.FINISH
  );

  const hplc02 = hplcResponse.hplcList.filter(
    (item) =>
      item.hplcType === constants.HPLC02 && item.status !== constants.FINISH
  );

  const hplc03 = hplcResponse.hplcList.filter(
    (item) =>
      item.hplcType === constants.HPLC03 && item.status !== constants.FINISH
  );

  const hplc04 = hplcResponse.hplcList.filter(
    (item) =>
      item.hplcType === constants.HPLC04 && item.status !== constants.FINISH
  );

  const renderHplc = (hplcList: TypeHplc[]) => {
    const liveDisplay: TypeHplc[] = [];
    const findFirstIndex = 0;

    const statusOngoing = hplcList.filter(
      (item) => item.status === constants.ONGOING
    )[findFirstIndex];

    const statusQueue = hplcList.filter(
      (item) => item.status === constants.QUEUE
    )[findFirstIndex];

    if (statusOngoing) {
      liveDisplay.push(statusOngoing);
    }

    if (statusQueue) {
      liveDisplay.push(statusQueue);
    }

    return liveDisplay.map((item) => (
      <ListGroup key={item.id}>
        <ListGroup.Item>
          <b>{item.status}</b>
        </ListGroup.Item>
        <ListGroup.Item>
          HPLC TYPE : <b>{item.hplcType}</b>
        </ListGroup.Item>
        <ListGroup.Item>
          Tester : <b>{item.tester}</b>
        </ListGroup.Item>
        <ListGroup.Item>
          Start : <b>{item.start}</b>
        </ListGroup.Item>
        <ListGroup.Item>
          End : <b>{item.end}</b>
        </ListGroup.Item>
        <ListGroup.Item>
          LOT : <b>{item.lot}</b>
        </ListGroup.Item>
        <ListGroup.Item>
          Sample : <b>{item.sample}</b>
        </ListGroup.Item>
      </ListGroup>
    ));
  };

  useEffect(() => {
    document.title = "Live Data";
    dispatch(getHplc());
  }, []);

  return (
    <Fragment>
      <Header />
      <Container>
        <Row sm={2} className="my-3">
          {hplc01.length !== 0 && hplc01.length && (
            <Col className="mb-3">
              <h3 className="text-center mb-3">{constants.HPLC01}</h3>
              <Row sm={2}>{renderHplc(hplc01)}</Row>
            </Col>
          )}

          {hplc02.length !== 0 && (
            <Col>
              <h3 className="text-center mb-3">{constants.HPLC02}</h3>
              <Row sm={2}>{renderHplc(hplc02)}</Row>
            </Col>
          )}

          {hplc03.length !== 0 && (
            <Col>
              <h3 className="text-center mb-3">{constants.HPLC03}</h3>
              <Row sm={2}>{renderHplc(hplc03)}</Row>
            </Col>
          )}

          {hplc04.length !== 0 && (
            <Col>
              <h3 className="text-center mb-3">{constants.HPLC04}</h3>
              <Row sm={2}>{renderHplc(hplc04)}</Row>
            </Col>
          )}
        </Row>
      </Container>
    </Fragment>
  );
};

export default LiveDisplay;
