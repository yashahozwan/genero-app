import React, { useEffect, useState } from "react";
import { Button, Container, Form, Table } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteHplc,
  getHplc,
  selectHplcData,
  updateHplc,
} from "../../store/hplcSlice";
import constants, { titleTableHead } from "../../constants/constants";
import TableHeadHplc from "../../components/table-head-hplc/TableHeadHplc";
import TypeHplc from "../../types/TypeHplc";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faSave, faTrash } from "@fortawesome/free-solid-svg-icons";
import { AppDispatch } from "../../store/configureStore";

const statusList = [constants.ONGOING, constants.QUEUE, constants.FINISH];

const HplcUpdate: React.FC = () => {
  const hplcResponse = useSelector(selectHplcData);
  const dispatch = useDispatch<AppDispatch>();
  const [editHplc, setEditHplc] = useState<TypeHplc>({
    createdAt: "",
    end: "",
    hplcType: "",
    id: 0,
    lot: "",
    sample: "",
    start: "",
    status: "",
    tester: "",
  });

  const hplc01 = hplcResponse.hplcList.filter(
    (item) =>
      item.hplcType === constants.HPLC01 && item.status !== constants.FINISH
  );

  const hplc02 = hplcResponse.hplcList.filter(
    (item) =>
      item.hplcType === constants.HPLC02 && item.status !== constants.FINISH
  );

  const hplc03 = hplcResponse.hplcList.filter(
    (item) =>
      item.hplcType === constants.HPLC03 && item.status !== constants.FINISH
  );

  const hplc04 = hplcResponse.hplcList.filter(
    (item) =>
      item.hplcType === constants.HPLC04 && item.status !== constants.FINISH
  );

  const handleEditHplc = () => {
    dispatch(updateHplc(editHplc));
    console.log(editHplc);
    handleCancel();
    setTimeout(() => {
      dispatch(getHplc());
    }, 500);
  };

  const handleDeleteHplc = (id: any) => {
    const sure = window.confirm("Apakah ingin menghapus data ini");
    if (sure) {
      dispatch(deleteHplc(id));
      setTimeout(() => {
        dispatch(getHplc());
      }, 500);
    }
  };

  const handleCancel = () => {
    setEditHplc({
      createdAt: "",
      end: "",
      hplcType: "",
      id: 0,
      lot: "",
      sample: "",
      start: "",
      status: "",
      tester: "",
    });
  };

  const renderOptionStatus = statusList.map((item) => (
    <option key={item} value={item}>
      {item}
    </option>
  ));

  const renderTableBody = (hplcList: TypeHplc[]) => {
    return hplcList.map((item) => (
      <tr key={item.id}>
        <td>{item.id}</td>
        <td>{item.hplcType}</td>
        <td>{item.tester}</td>
        <td>
          {editHplc.id === item.id ? (
            <Form.Group>
              <Form.Control
                value={editHplc.start}
                onChange={(event) =>
                  setEditHplc({ ...editHplc, start: event.currentTarget.value })
                }
              />
            </Form.Group>
          ) : (
            <span>{item.start}</span>
          )}
        </td>
        <td>
          {editHplc.id === item.id ? (
            <Form.Group>
              <Form.Control
                value={editHplc.end}
                onChange={(event) =>
                  setEditHplc({ ...editHplc, end: event.currentTarget.value })
                }
              />
            </Form.Group>
          ) : (
            <span>{item.end}</span>
          )}
        </td>
        <td>{item.sample}</td>
        <td>{item.lot}</td>
        <td className="d-flex">
          {item.id === editHplc.id ? (
            <Form.Select
              className="me-2"
              onChange={(event) =>
                setEditHplc({ ...editHplc, status: event.currentTarget.value })
              }
            >
              <option>Status</option>
              {renderOptionStatus}
            </Form.Select>
          ) : (
            <span className="flex-grow-1">{item.status}</span>
          )}

          {item.id === editHplc.id ? (
            <Button className="me-1" onClick={() => handleEditHplc()}>
              <FontAwesomeIcon icon={faSave} />
            </Button>
          ) : (
            <Button className="me-1" onClick={() => setEditHplc(item)}>
              <FontAwesomeIcon icon={faEdit} />
            </Button>
          )}

          <Button variant="danger" onClick={() => handleDeleteHplc(item.id)}>
            <FontAwesomeIcon icon={faTrash} />
          </Button>
        </td>
      </tr>
    ));
  };

  useEffect(() => {
    document.title = "HPLC Update";
    dispatch(getHplc());
  }, []);

  return (
    <section>
      <Container>
        <Table striped bordered hover>
          <TableHeadHplc titleHead={titleTableHead} title={constants.HPLC01} />
          <tbody>{renderTableBody(hplc01)}</tbody>
        </Table>
        <Table striped bordered hover>
          <TableHeadHplc titleHead={titleTableHead} title={constants.HPLC02} />
          <tbody>{renderTableBody(hplc02)}</tbody>
        </Table>
        <Table striped bordered hover>
          <TableHeadHplc titleHead={titleTableHead} title={constants.HPLC03} />
          <tbody>{renderTableBody(hplc03)}</tbody>
        </Table>
        <Table striped bordered hover>
          <TableHeadHplc titleHead={titleTableHead} title={constants.HPLC04} />
          <tbody>{renderTableBody(hplc04)}</tbody>
        </Table>
      </Container>
    </section>
  );
};

export default HplcUpdate;
