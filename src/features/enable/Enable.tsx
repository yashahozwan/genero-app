import React, { useEffect, useState } from "react";
import { Alert, Button, Container, Form } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch } from "../../store/configureStore";
import {
  selectAllPagesData,
  getPageSignup,
  setPageSignup,
} from "../../store/pageSignupSlice";
import { selectUsersData } from "../../store/usersSlice";
import constants from "../../constants/constants";
import { Navigate } from "react-router-dom";
import endpoint from "../../constants/endpoint";

const Enable: React.FC = () => {
  const dispatch = useDispatch<AppDispatch>();
  const pageResponse = useSelector(selectAllPagesData);
  const userProfile = useSelector(selectUsersData);
  const [isEnable, setIsEnable] = useState(false);

  const handleOnClick = () => {
    setIsEnable(!isEnable);

    setTimeout(() => {
      dispatch(setPageSignup({ isEnable }));
    }, 100);

    setTimeout(() => {
      dispatch(getPageSignup());
    }, 500);
  };

  const handleVariant = () => {
    return pageResponse.data.isEnable ? "primary" : "danger";
  };

  const handleTitle = () => {
    const title = pageResponse.data.isEnable ? "Open" : "Close";
    return `Registration ${title}`;
  };

  const handleButtonTitle = () => {
    return pageResponse.data.isEnable ? "Enable" : "Disable";
  };

  useEffect(() => {
    document.title = "Enable Registration";
  }, []);

  if (userProfile.data.role !== constants.ADMIN) {
    return <Navigate to={endpoint.root} />;
  }

  return (
    <section>
      <Container>
        <h3>Enable Registration</h3>
        <p>{handleTitle()}</p>
        <Form>
          <Alert variant={handleVariant()}>
            <Button variant={handleVariant()} onClick={handleOnClick}>
              {handleButtonTitle()}
            </Button>
          </Alert>
        </Form>
      </Container>
    </section>
  );
};

export default Enable;
