import React, { useEffect } from "react";
import { Card, Container, ListGroup } from "react-bootstrap";
import { useSelector } from "react-redux";
import { selectUsersData } from "../../store/usersSlice";
import dateFormatter from "../../utils/dateFormatter";
import constants from "../../constants/constants";
import { Navigate } from "react-router-dom";
import endpoint from "../../constants/endpoint";

const Profile: React.FC = () => {
  const token = localStorage.getItem(constants.Token);
  const userProfile = useSelector(selectUsersData);

  useEffect(() => {
    document.title = "Profile";
  }, []);

  if (!token) {
    return <Navigate to={endpoint.signIn} />;
  }

  return (
    <section>
      <Container>
        <Card>
          <Card.Body>
            <Card.Title>
              {!userProfile.error && userProfile.data.name}
            </Card.Title>
            <ListGroup variant="flush">
              <ListGroup.Item>
                Email : {!userProfile.error && userProfile.data.email}
              </ListGroup.Item>
              <ListGroup.Item>
                Level : {!userProfile.error && userProfile.data.role}
              </ListGroup.Item>
              <ListGroup.Item>
                Created At :
                {!userProfile.error &&
                  dateFormatter(userProfile.data.createdAt)}
              </ListGroup.Item>
            </ListGroup>
          </Card.Body>
        </Card>
      </Container>
    </section>
  );
};

export default Profile;
