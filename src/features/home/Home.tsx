import "./Home.css";
import React, { Fragment, useEffect } from "react";
import { Button } from "react-bootstrap";
import { Navigate, Outlet, useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSignOut } from "@fortawesome/free-solid-svg-icons";

import constants from "../../constants/constants";
import endpoint from "../../constants/endpoint";
import { useDispatch, useSelector } from "react-redux";
import { AppDispatch } from "../../store/configureStore";
import { authSignOut } from "../../store/authSlice";
import { getUserProfile, selectUsersData } from "../../store/usersSlice";
import Header from "../../components/header/Header";
import NavAdmin from "../../components/nav/NavAdmin";
import NavAccount from "../../components/nav/NavAccount";

const Home: React.FC = () => {
  const token = localStorage.getItem(constants.Token);
  const dispatch = useDispatch<AppDispatch>();
  const navigate = useNavigate();
  const usersResponse = useSelector(selectUsersData);

  const handleSignOut = () => {
    navigate(endpoint.signIn);
    dispatch(authSignOut());
  };

  useEffect(() => {
    document.title = "Home";
    dispatch(getUserProfile());
  }, []);

  if (!token) {
    return <Navigate to={endpoint.signIn} />;
  }

  return (
    <Fragment>
      <Header />
      <NavAccount />
      {!usersResponse.error && usersResponse.data.role === constants.ADMIN && (
        <NavAdmin />
      )}
      <Outlet />
      <section>
        <Button className="floating-button" onClick={handleSignOut} size="lg">
          <FontAwesomeIcon icon={faSignOut} />
        </Button>
      </section>
    </Fragment>
  );
};

export default Home;
