import "./NotFound.css";
import React, { Fragment } from "react";
import Header from "../../components/header/Header";
import ImageNotFound from "../../assets/image/not-found.svg";

const NotFound: React.FC = () => {
  return (
    <Fragment>
      <Header />
      <div className="not-found-section">
        <img className="not-found-img" src={ImageNotFound} alt="image" />
      </div>
    </Fragment>
  );
};

export default NotFound;
