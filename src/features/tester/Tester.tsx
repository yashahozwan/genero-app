import React, { SyntheticEvent, useEffect, useState } from "react";
import {
  Alert,
  Button,
  Col,
  Container,
  Form,
  InputGroup,
  ListGroup,
  ListGroupItem,
  Row,
} from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSave,
  faEdit,
  faTrash,
  faBan,
  faX,
} from "@fortawesome/free-solid-svg-icons";
import { useDispatch, useSelector } from "react-redux";
import {
  createTester,
  deleteTester,
  getAllTesters,
  getTesters,
  updateTester,
} from "../../store/testersSlice";
import { AppDispatch } from "../../store/configureStore";
import TypeUpdateTester from "../../types/TypeUpdateTester";
import { selectUsersData } from "../../store/usersSlice";
import constants from "../../constants/constants";
import { Navigate } from "react-router-dom";
import endpoint from "../../constants/endpoint";

const TIME_DELAY_FETCH = 200;
const Tester: React.FC = () => {
  const dispatch = useDispatch<AppDispatch>();
  const testersResponse = useSelector(getAllTesters);
  const userProfile = useSelector(selectUsersData);
  const [name, setName] = useState<string>("");
  const [editTester, setEditTester] = useState<TypeUpdateTester>({
    id: 0,
    name: "",
  });

  const onSubmit = (event: SyntheticEvent) => {
    event.preventDefault();
    const data = { name };
    dispatch(createTester(data));

    setTimeout(() => {
      dispatch(getTesters());
    }, TIME_DELAY_FETCH);

    setName("");
  };

  const handleUpdateTester = () => {
    dispatch(updateTester(editTester));

    setTimeout(() => {
      dispatch(getTesters());
    }, TIME_DELAY_FETCH);

    setEditTester({ id: 0, name: "" });
  };

  const handleDeleteTester = (id: number) => {
    const onDelete = window.confirm("Do you want to delete this?");
    if (onDelete) {
      const data = { id };
      dispatch(deleteTester(data));

      setTimeout(() => {
        dispatch(getTesters());
      }, TIME_DELAY_FETCH);
    }
  };

  useEffect(() => {
    document.title = "Testers";
    dispatch(getTesters());
  }, []);

  const renderListTester = testersResponse.data.map((tester) => {
    return (
      <ListGroupItem key={tester.id}>
        <div>
          {editTester.id === tester.id ? (
            <Form.Group>
              <InputGroup className="mb-3">
                <Form.Control
                  value={editTester.name}
                  onChange={(event) => {
                    setEditTester({
                      ...editTester,
                      name: event.currentTarget.value,
                    });
                  }}
                />
                <Button onClick={() => setEditTester({ id: 0, name: "" })}>
                  <FontAwesomeIcon icon={faX} />
                </Button>
                <Button onClick={handleUpdateTester}>
                  <FontAwesomeIcon icon={faSave} />
                </Button>
              </InputGroup>
            </Form.Group>
          ) : (
            <p>
              <b>{tester.name}</b>
            </p>
          )}

          <Button
            className="me-2"
            onClick={() => {
              setEditTester({ id: tester.id, name: tester.name });
            }}
          >
            <FontAwesomeIcon icon={faEdit} />
          </Button>
          <Button onClick={handleDeleteTester.bind(this, tester.id)}>
            <FontAwesomeIcon icon={faTrash} />
          </Button>
        </div>
      </ListGroupItem>
    );
  });

  if (userProfile.data.role !== constants.ADMIN) {
    return <Navigate to={endpoint.root} />;
  }

  return (
    <section>
      <Container>
        <h3>Manage Tester</h3>

        <Form onSubmit={onSubmit} className="mb-3">
          <Form.Group>
            <InputGroup>
              <Form.Control
                type="text"
                value={name}
                onChange={(event) => setName(event.currentTarget.value)}
              />
              <Button type="submit" disabled={name.length < 3}>
                <FontAwesomeIcon icon={faSave} /> Save
              </Button>
            </InputGroup>
          </Form.Group>
        </Form>
        <ListGroup>{renderListTester}</ListGroup>
      </Container>
    </section>
  );
};

export default Tester;
