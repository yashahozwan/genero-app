import "./SignIn.css";
import React, { SyntheticEvent, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, Navigate, useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Alert, Button, Form, FormControl, InputGroup } from "react-bootstrap";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";

import { AppDispatch } from "../../store/configureStore";
import {
  authCleaning,
  authSignIn,
  selectAuthData,
} from "../../store/authSlice";
import Logo from "../../assets/image/logo.png";
import constants from "../../constants/constants";
import endpoint from "../../constants/endpoint";
import { selectAllPagesData } from "../../store/pageSignupSlice";

export const SignIn: React.FC = () => {
  const token = localStorage.getItem(constants.Token);
  const dispatch = useDispatch<AppDispatch>();
  const authResponse = useSelector(selectAuthData);
  const pagesResponse = useSelector(selectAllPagesData);
  const naviate = useNavigate();

  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [isVisible, setIsVisible] = useState<boolean>(false);

  const onSubmit = (event: SyntheticEvent) => {
    event.preventDefault();
    dispatch(authSignIn({ email, password }));

    setTimeout(() => {
      if (authResponse.data.token) {
        localStorage.setItem(constants.Token, authResponse.data.token);
        naviate(endpoint.root);
      }
    }, 200);
  };

  useEffect(() => {
    document.title = "Sign In";
  }, []);

  if (token) {
    return <Navigate to={endpoint.root} />;
  }

  return (
    <section className="sign-in-section">
      <form onSubmit={onSubmit}>
        <img src={Logo} alt="logo" />
        <Form.Group>
          {authResponse.message && (
            <Alert variant="danger">{authResponse.message}</Alert>
          )}
          <Form.Label>Email</Form.Label>
          <Form.Control
            className="mb-3"
            type="text"
            value={email}
            onChange={(event) => setEmail(event.currentTarget.value)}
          />
        </Form.Group>
        <Form.Group className="mb-4">
          <Form.Label>Password</Form.Label>
          <InputGroup className="mb-3">
            <FormControl
              aria-describedby="basic-addon2"
              type={!isVisible ? "password" : "text"}
              value={password}
              onChange={(event) => setPassword(event.currentTarget.value)}
            />
            <Button
              variant="outline-primary"
              id="button-addon2"
              onClick={() => setIsVisible(!isVisible)}
            >
              <FontAwesomeIcon icon={!isVisible ? faEye : faEyeSlash} />
            </Button>
          </InputGroup>
        </Form.Group>
        <Button
          disabled={!(email && password)}
          type="submit"
          className="sign-in-button mb-3"
        >
          Sign In
        </Button>

        {pagesResponse.data.isEnable ? (
          <p className="sign-in-text-button">
            <Link onClick={() => dispatch(authCleaning())} to="/signup">
              Register
            </Link>
          </p>
        ) : null}
      </form>
    </section>
  );
};

export default SignIn;
