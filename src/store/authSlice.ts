import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

import generoApi from "../api/generoApi";
import TypeSignIn from "../types/TypeSignIn";
import AuthResponse from "../data/authResponse";
import TypeSignInResponse from "../types/TypeSignInResponse";
import TypeSignUpResponse from "../types/TypeSignUpResponse";
import TypeSignUp from "../types/TypeSignUp";
import constants from "../constants/constants";

const initialState: AuthResponse = {
  error: true,
  loading: true,
  message: null,
  data: {
    token: "",
    id: 0,
    name: "",
    email: "",
    password: "",
    createdAt: "",
  },
};

const authSlice = createSlice({
  name: "authentication",
  initialState,
  reducers: {
    authCleaning: (state) => {
      state.message = null;
    },

    authSignOut: (state) => {
      state.error = true;
      localStorage.removeItem(constants.Token);
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(authSignIn.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(authSignIn.fulfilled, (state, action) => {
        state.loading = false;
        state.error = action.payload.error;
        state.message = action.payload.message;
        state.data = action.payload.data;

        console.log(action.payload.data);
      })
      .addCase(authSignUp.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(authSignUp.fulfilled, (state, action) => {
        state.loading = false;
        state.error = action.payload.error;
        state.message = action.payload.message;
        state.data = action.payload.data;
      });
  },
});

export const authSignIn = createAsyncThunk<TypeSignInResponse, TypeSignIn, {}>(
  "auth/signin",
  async (data) => {
    try {
      const response = await generoApi.post("/auth/signin", data);
      return response.data;
    } catch (error: any) {
      return error.response.data;
    }
  }
);

export const authSignUp = createAsyncThunk<TypeSignUpResponse, TypeSignUp, {}>(
  "/auth/signup",
  async (data) => {
    try {
      const response = await generoApi.post("/auth/signup", data);
      return response.data;
    } catch (error: any) {
      return error.response.data;
    }
  }
);

export const selectAuthData = (state: { auth: AuthResponse }) => state.auth;

export const { authCleaning, authSignOut } = authSlice.actions;

export default authSlice.reducer;
