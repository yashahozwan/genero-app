import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import generoApi from "../api/generoApi";
import PagesResponse from "../data/pagesResponse";

const initialState: PagesResponse = {
  loading: true,
  error: true,
  message: "",
  data: {
    id: 0,
    name: "",
    isEnable: false,
  },
};

const pageSignupSlice = createSlice({
  name: "pages",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getPageSignup.pending, (state) => {
        state.loading = true;
      })

      .addCase(getPageSignup.fulfilled, (state, action) => {
        state.loading = false;
        state.error = action.payload.error;
        state.message = action.payload.message;
        state.data = action.payload.data;
      })

      .addCase(setPageSignup.pending, (state) => {
        state.loading = true;
      })

      .addCase(setPageSignup.fulfilled, (state, action) => {
        state.loading = false;
        state.error = action.payload.error;
        state.message = action.payload.message;
        state.data = action.payload.data;
      });
  },
});

export const getPageSignup = createAsyncThunk(
  "pages/getPageSignup",
  async () => {
    try {
      const response = await generoApi.get("/pages");
      return response.data;
    } catch (error: any) {
      return error.response.data;
    }
  }
);

export const setPageSignup = createAsyncThunk(
  "pages/setPageSetup",
  async (data: any) => {
    try {
      const response = await generoApi.patch("/pages", data);
      return response.data;
    } catch (error: any) {
      return error.response.data;
    }
  }
);

export const selectAllPagesData = (state: { pages: PagesResponse }) =>
  state.pages;

export default pageSignupSlice.reducer;
