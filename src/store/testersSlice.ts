import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import generoApi from "../api/generoApi";
import TestersResponse from "../data/testersResponse";
import typeDeleteTester from "../types/TypeDeleteTester";
import TypeDeleteTester from "../types/TypeDeleteTester";
import { useNavigate } from "react-router-dom";

const initialState: TestersResponse = {
  loading: true,
  error: false,
  message: null,
  data: [],
};

const testersSlice = createSlice({
  name: "testers",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getTesters.pending, (state) => {
        state.loading = true;
      })

      .addCase(getTesters.fulfilled, (state, action) => {
        state.loading = false;
        state.error = action.payload.error;
        state.message = action.payload.message;
        state.data = action.payload.data;
      })

      .addCase(createTester.pending, (state) => {
        state.loading = true;
      })

      .addCase(createTester.fulfilled, (state, action) => {
        state.loading = false;
        state.error = action.payload.error;
        state.message = action.payload.message;
      })

      .addCase(updateTester.pending, (state) => {
        state.loading = true;
      })

      .addCase(updateTester.fulfilled, (state, action) => {
        state.loading = false;
        state.error = action.payload.error;
        state.message = action.payload.message;
      });
  },
});

export const createTester = createAsyncThunk(
  "testers/create",
  async (data: any) => {
    try {
      const response = await generoApi.post("/testers", data);
      return response.data;
    } catch (error: any) {
      return error.response.data;
    }
  }
);

export const getTesters = createAsyncThunk("testers/getTesters", async () => {
  try {
    const response = await generoApi.get("/testers");
    return response.data;
  } catch (error: any) {
    return error.response.data;
  }
});

export const getAllTesters = (state: { testers: TestersResponse }) =>
  state.testers;

export const updateTester = createAsyncThunk(
  "testers/update",
  async (data: any) => {
    try {
      const response = await generoApi.patch("/testers/update", data);
      return response.data;
    } catch (error: any) {
      return error.response.data;
    }
  }
);

export const deleteTester = createAsyncThunk(
  "testers/delete",
  async (data: any) => {
    try {
      const response = await generoApi.delete("/testers/delete/" + data.id);
      return response.data;
    } catch (error: any) {
      return error.response.data;
    }
  }
);

export const {} = testersSlice.actions;

export default testersSlice.reducer;
