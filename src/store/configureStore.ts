import { Action, configureStore, ThunkAction } from "@reduxjs/toolkit";
import authReducer from "./authSlice";
import usersReducer from "./usersSlice";
import testersReducer from "./testersSlice";
import pagesReducer from "./pageSignupSlice";
import hplcReducer from "./hplcSlice";

const store = configureStore({
  reducer: {
    auth: authReducer,
    users: usersReducer,
    testers: testersReducer,
    pages: pagesReducer,
    hplc: hplcReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;

export default store;
