import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import generoApi from "../api/generoApi";
import UsersResponse from "../data/usersResponse";
import usersResponse from "../data/usersResponse";
import constants from "../constants/constants";
import { useNavigate } from "react-router-dom";

const initialState: UsersResponse = {
  data: { createdAt: "", email: "", id: 0, name: "", role: "" },
  error: false,
  loading: false,
  message: null,
  users: [],
};

const usersSlice = createSlice({
  name: "users",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getUserProfile.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(getUserProfile.fulfilled, (state, action) => {
        state.loading = false;
        state.error = action.payload.error;
        state.message = action.payload.message;
        state.data = action.payload.data;
        state.users = [];
      })
      .addCase(getUsers.pending, (state) => {
        state.loading = true;
      })
      .addCase(getUsers.fulfilled, (state, action) => {
        state.loading = false;
        state.error = action.payload.error;
        state.message = action.payload.message;
        state.users = action.payload.data;
      })
      .addCase(deleteUser.pending, (state) => {
        state.loading = true;
      })
      .addCase(deleteUser.fulfilled, (state) => {
        state.loading = false;
      });
  },
});

export const getUserProfile = createAsyncThunk("users/profile", async () => {
  try {
    const response = await generoApi.get("/users/profile");
    return response.data;
  } catch (error: any) {
    return error.response.data;
  }
});

export const getUsers = createAsyncThunk("users/getUsers", async () => {
  try {
    const response = await generoApi.get("/users");
    return response.data;
  } catch (error: any) {
    return error.response.data;
  }
});

export const deleteUser = createAsyncThunk("users/delete", async (id: any) => {
  try {
    await generoApi.delete("/users/" + id);
  } catch (error: any) {
    return error.response.data;
  }
});

export const selectUsersData = (state: { users: UsersResponse }) => state.users;

export default usersSlice.reducer;
