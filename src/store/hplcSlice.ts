import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import generoApi from "../api/generoApi";
import HplcResponse from "../data/hplcResponse";

const initialState: HplcResponse = {
  data: {},
  error: "",
  hplcList: [],
  loading: true,
  message: "",
};

const hplcSlice = createSlice({
  name: "hplc",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getHplc.pending, (state) => {
        state.loading = true;
      })
      .addCase(getHplc.fulfilled, (state, action) => {
        state.loading = false;
        state.error = action.payload.error;
        state.message = action.payload.message;
        state.hplcList = action.payload.data;
      })
      .addCase(createHplc.pending, (state) => {
        state.loading = true;
      })
      .addCase(createHplc.fulfilled, (state, action) => {
        state.loading = false;
        state.message = action.payload.message;
      })
      .addCase(deleteHplc.pending, (state) => {
        state.loading = true;
      })
      .addCase(deleteHplc.fulfilled, (state, action) => {
        state.loading = false;
        state.message = action.payload.message;
      });
  },
});

export const getHplc = createAsyncThunk("hplc/getHplc", async () => {
  try {
    const response = await generoApi.get("/hplc");
    return response.data;
  } catch (error: any) {
    return error.response.data;
  }
});

export const createHplc = createAsyncThunk(
  "hplc/createHplc",
  async (data: any) => {
    try {
      const response = await generoApi.post("/hplc", data);
      return response.data;
    } catch (error: any) {
      return error.response.data;
    }
  }
);

export const updateHplc = createAsyncThunk(
  "hplc/updateHplc",
  async (data: any) => {
    try {
      const response = await generoApi.patch(`/hplc/${data.id}`, data);
      return response.data;
    } catch (error: any) {
      return error.response.data;
    }
  }
);

export const deleteHplc = createAsyncThunk(
  "hplc/deleteHplc",
  async (id: any) => {
    try {
      const response = await generoApi.delete(`/hplc/${id}`);
      return response.data;
    } catch (error: any) {
      return error.response.data;
    }
  }
);

export const selectHplcData = (state: { hplc: HplcResponse }) => state.hplc;

export default hplcSlice.reducer;
