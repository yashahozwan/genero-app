type TypeUser = {
  id: number;
  name: string;
  email: string;
  password: string;
  createdAt: string;
};

export default TypeUser;
