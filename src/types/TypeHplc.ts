type TypeHplc = {
  id: number;
  hplcType: string;
  tester: string;
  start: string;
  end: string;
  sample: string;
  lot: string;
  status: string;
  createdAt: string;
};

export default TypeHplc;
