type TypeSignInResponse = {
  error: boolean;
  message: string;
  data: {
    token: string;
    id: number;
    name: string;
    email: string;
    password: string;
    createdAt: string;
  };
};

export default TypeSignInResponse;
