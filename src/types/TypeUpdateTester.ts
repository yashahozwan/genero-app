type TypeUpdateTester = {
  id: number;
  name: string;
};

export default TypeUpdateTester;
