type TypeSignUp = {
  name: string;
  email: string;
  password: string;
};

export default TypeSignUp;
