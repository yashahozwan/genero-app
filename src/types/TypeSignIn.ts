type TypeSignIn = {
  email: string;
  password: string;
};

export default TypeSignIn;
