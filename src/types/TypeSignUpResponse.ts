type TypeSignUpResponse = {
  error: boolean;
  message: string | null;
  data: {
    token: string;
    id: number;
    name: string;
    email: string;
    password: string;
    createdAt: string;
  };
};

export default TypeSignUpResponse;
