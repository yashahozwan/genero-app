type TypeTester = {
  id: number;
  name: string;
  userId: number;
  createdAt: string;
};

export default TypeTester;
