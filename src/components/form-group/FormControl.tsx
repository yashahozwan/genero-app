import React, { ChangeEventHandler } from "react";
import { Form } from "react-bootstrap";

type MyFormControl = {
  onChange: ChangeEventHandler<HTMLInputElement>;
  value: string;
  mb: number;
  placeholder: string;
};

const FormControl: React.FC<MyFormControl> = (props) => {
  return (
    <Form.Group className={`mb-${props.mb}`}>
      <Form.Control
        value={props.value}
        onChange={props.onChange}
        placeholder={props.placeholder}
      />
    </Form.Group>
  );
};

export default FormControl;
