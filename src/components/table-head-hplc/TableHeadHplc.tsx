import React from "react";
import { Table } from "react-bootstrap";

const titleHead = [
  "Id",
  "HPLC Type",
  "Tester",
  "Start",
  "End",
  "Sample",
  "LOT",
  "Status",
];

type TableHead = {
  title: string;
  titleHead: string[];
};

const TableHeadHplc: React.FC<TableHead> = (props) => {
  const renderTableHead = props.titleHead.map((item) => (
    <th key={item}>{item}</th>
  ));
  return (
    <thead>
      <tr>
        <th style={{ textAlign: "center" }} colSpan={props.titleHead.length}>
          {props.title}
        </th>
      </tr>
      <tr>{renderTableHead}</tr>
    </thead>
  );
};

export default TableHeadHplc;
