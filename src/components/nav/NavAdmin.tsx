import "./Nav.css";
import React from "react";
import { Link } from "react-router-dom";
import { Container } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faAdd,
  faHome,
  faUsers,
  faGear,
  faList,
  faUser,
} from "@fortawesome/free-solid-svg-icons";
import endpoint from "../../constants/endpoint";

const menu = [
  { value: "Manage Tester", path: endpoint.tester, icon: faList },
  { value: "Manage Account", path: endpoint.users, icon: faUsers },
  { value: "Enable Registration", path: endpoint.enable, icon: faGear },
];

const NavAdmin: React.FC = () => {
  const renderMenu = () =>
    menu.map((item) => (
      <li key={item.value} className="list-item">
        <Link to={item.path} className="list-item-link">
          <FontAwesomeIcon className="fa-icon" icon={item.icon} />
          {item.value}
        </Link>
      </li>
    ));

  return (
    <div className="nav-container">
      <Container>
        <nav className="nav">
          <ul className="list-container">{renderMenu()}</ul>
        </nav>
      </Container>
    </div>
  );
};

export default NavAdmin;
