import "./Nav.css";
import React from "react";
import { Container } from "react-bootstrap";
import endpoint from "../../constants/endpoint";
import {
  faDisplay,
  faEdit,
  faGear,
  faHistory,
  faHome,
  faList,
  faUser,
  faUsers,
} from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const menu = [
  { value: "Home", path: endpoint.root, icon: faHome },
  { value: "HPLC", path: endpoint.hplc, icon: faEdit },
  { value: "History", path: endpoint.history, icon: faHistory },
  { value: "Live Display", path: endpoint.liveDisplay, icon: faDisplay },
  { value: "Profile", path: endpoint.profile, icon: faUser },
];

const NavAccount: React.FC = () => {
  const renderMenu = () =>
    menu.map((item) => (
      <li key={item.value} className="list-item">
        <Link to={item.path} className="list-item-link">
          <FontAwesomeIcon className="fa-icon" icon={item.icon} />
          {item.value}
        </Link>
      </li>
    ));

  return (
    <div className="nav-container">
      <Container>
        <nav className="nav">
          <ul className="list-container">{renderMenu()}</ul>
        </nav>
      </Container>
    </div>
  );
};

export default NavAccount;
