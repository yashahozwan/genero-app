import "./Header.css";
import React from "react";
import TopIcon from "../../assets/image/top.png";

const Header: React.FC = () => {
  return (
    <header className="header">
      <img className="genero-top-icon" src={TopIcon} alt="logo" />
    </header>
  );
};

export default Header;
